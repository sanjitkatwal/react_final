import React, {useState} from 'react';


let Counter = () =>{
    let [state, setState] = useState({
        count: 0
    });

    let incrCount = () => {
        setState({
            count:state.count +1
        })
    };
    let decrCount = () => {
        setState({
            count:state.count -1 > 0 ? state.count -1 : 0
        })
    }
    return (
        <React.Fragment>
            <section>
                <div className="container">
                    <div className="row">
                        <div className="col-md-3 mt-3">
                            <div className="card">
                                <div className="card-header bg-info">
                                    <p className="h4">Counter</p>
                                </div>
                                <div className="card-body bg-light">
                                    <h5 className='display-3 m-5'>{state.count}</h5>
                                    <button onClick={incrCount} className="btn btn-success btn-sm m-2">Increment</button>
                                    <button onClick={decrCount} className="btn btn-warning btn-sm">Decrement</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </React.Fragment>
    )
}


export default Counter