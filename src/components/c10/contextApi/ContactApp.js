import React from 'react';
import ContactList from "./ContactList";
import ContactCard from "./ContactCard";


let ContactApp = ({passContact}) =>{
    console.log(passContact)
    return (
        <React.Fragment>
            <nav className="navbar navbar-dark bg-dark navbar-expand-sm p-3">
                <a href="/" className='navbar-brand'>Contact App</a>
            </nav>
            <div className="container mt-3">
                <div className="row">
                    <div className="col">
                        <p className="h4 txt-success">Contact App</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et eum laboriosam sunt temporibus
                            unde? A animi distinctio dolore est ipsa, iure nam natus, necessitatibus pariatur
                            perferendis ratione reiciendis repellendus sint.</p>
                    </div>
                </div>
            </div>
            <div className="container mt-3">
                <div className="row">
                    <div className="col-md-9">
                        <ContactList passContact={passContact} />
                    </div>
                    <div className="col-md-3">
                        <ContactCard />

                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

export default ContactApp