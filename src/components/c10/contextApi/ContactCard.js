import React, {useContext} from 'react';
import UserContext from "./context/UserContext";


let ContactCard = () =>{
    let contactState = useContext(UserContext)
    let {selectedContact} = contactState
    return (
        <React.Fragment>
            <h2>Contact Card</h2>
            {/*<pre>{JSON.stringify(selectedContact)}</pre>*/}
            {
                Object.keys(selectedContact).length > 0 &&
                <div className='card sticky-top'>
                    <div className="card-header bg-success text-white text-center">
                        <img src={selectedContact.picture.large} alt="" className='rounded-circle'/>
                    </div>
                    <div className="card-body">
                        <ul className="list-group">
                            <li className="list-group-item">
                                Name: {selectedContact.name.first} {selectedContact.name.last}
                            </li>
                            <li className="list-group-item">
                                Age: {selectedContact.dob.age}
                            </li>
                            <li className="list-group-item">
                                City: {selectedContact.location.city}
                            </li>
                            <li className="list-group-item">
                                Country: {selectedContact.location.country}
                            </li>
                        </ul>
                    </div>
                </div>
            }
        </React.Fragment>
    )
}

export default ContactCard