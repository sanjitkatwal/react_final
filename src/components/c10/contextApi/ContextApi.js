import React, {useState} from 'react'
import {Contact} from "./Contact";
import ContactApp from "./ContactApp";
import UserContext from "./context/UserContext";


let ContextApi = () =>{

    let [state, setState] = useState({
        contacts: Contact,
        selectedContact:{}
    })

    //to receive the selected contact state from children
    let passContact = (contact) => {
        setState({
            ...state,
            selectedContact: contact
        })
    }
    return (

        <React.Fragment>
            {JSON.stringify(state.selectedContact)}
            <UserContext.Provider value={state}>
                <ContactApp passContact={passContact}/>
            </UserContext.Provider>

        </React.Fragment>

    );
}

export default ContextApi;
