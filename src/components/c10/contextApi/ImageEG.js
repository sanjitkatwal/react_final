import React from 'react';
import ContextApiImage from '../../../assets/images/c10/Capture.JPG'


let ImageEG = () =>{
    return (
        <React.Fragment>
            <nav className="navbar navbar-dark bg-dark navbar-expand-sm p-3">
                <a href="/" className='navbar-brand'>Context Api in Image</a>
            </nav>
            <p>
                <img src="{ContextApiImage}" alt=""/>
            </p>
        </React.Fragment>
    )
}

export default ImageEG