import React from "react";
import {Link, Route, Switch, BrowserRouter} from "react-router-dom"
import './c10-nav-style.css'
import ContextApi from "./ContextApi";
import ImageEG from "./ImageEG";



const C10nav = () => {
    return(
        <BrowserRouter>
            <h2>Im V9nav</h2>
            <ul className='nav'>
                <li>
                    <Link to='/context-api'>Context API</Link>
                </li>
                <li>
                    <Link to='/image-eg'>Image</Link>
                </li>

            </ul>
            <Switch>
                <Route path='/context-api' component={ContextApi}/>
                <Route path='/image-eg' component={ImageEG}/>
            </Switch>
        </BrowserRouter>

    )
}


export default C10nav