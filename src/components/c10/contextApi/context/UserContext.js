import React from "react";
import {Contact} from "../Contact";

const UserContext = React.createContext({
    contacts: [Contact],
    selectedContact: {Contact}
})


export default UserContext