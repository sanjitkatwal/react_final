import React from 'react';
import Card from "./functional/Card";
import Table from "./class/Table";


let ComponentApp = () =>{
    return (
        <React.Fragment>
            <h2 style={{color:"white", backgroundColor: '#f56d', borderRadius: '0px'}}>Class and Functional Component</h2>
            <p>In this section we see the two types of components defines in react. One is Functional Component and
            another one is Class Based Component</p>
            <Card />
            <Table />
        </React.Fragment>
    )
}

export default ComponentApp