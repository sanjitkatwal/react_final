import React from "react";
import './Table.css'


class Table extends React.Component{
    constructor() {
        super();
    }

    render(){
        return(
            <React.Fragment>
                <div className='mine-table'>
                    <h3 style={{color:"white", backgroundColor: 'blue', borderRadius: '0px'}}>React Class Based Component</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam at delectus deserunt, dolore ducimus
                        eum eveniet excepturi incidunt laborum magni natus non possimus quibusdam quidem quis quos recusandae
                        sit voluptatem.</p>
                </div>
            </React.Fragment>
        )
    }

}


export default Table