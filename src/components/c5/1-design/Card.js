import React from 'react';
import './Card.css'

let Card = () =>{
    return (
        <React.Fragment>
            <nav className="navbar">
                <a href="">React with CSS Style</a>
            </nav>
            <div className="card">
                <div className="card-body">
                    <h3>React Js</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad eius enim id iure minus nemo quidem
                        similique sit, tenetur veritatis. Ad, debitis dolore dolorem eos labore modi omnis porro voluptate.</p>
                </div>
            </div>
            <div className="card">
                <div className="card-body">
                    <h3>Vue Js</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad eius enim id iure minus nemo quidem
                        similique sit, tenetur veritatis. Ad, debitis dolore dolorem eos labore modi omnis porro voluptate.</p>
                </div>
            </div>

            <div className="card">
                <div className="card-body">
                    <h3>Angular Js</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad eius enim id iure minus nemo quidem
                        similique sit, tenetur veritatis. Ad, debitis dolore dolorem eos labore modi omnis porro voluptate.</p>
                </div>
            </div>
        </React.Fragment>
    )
}

export default Card