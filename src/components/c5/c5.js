import React from "react";
import {Link, Route, Switch, BrowserRouter} from "react-router-dom"
import './c5-style.css'
import Card from "./1-design/Card";
import Parent from "./employee/Parent";
import ParentState from "./state-eg/ParentState";
import ParentStateApp from "./multiple-user-render-state-eg/ParentStateApp";



const C5 = () => {
    return(
        <BrowserRouter>
            <ul className='nav'>
                <li>
                    <Link to='/css-example'>Simple css example</Link>
                </li>
                <li>
                    <Link to='/props-example'>Props Example</Link>
                </li>
                <li>
                    <Link to='/state-example'>State Example</Link>
                </li>
                <li>
                    <Link to='/state-example-multi-rendering'>State Example with Multiple User</Link>
                </li>
            </ul>
        <Switch>
            <Route path='/css-example' component={Card} />
            <Route path='/props-example' component={Parent} />
            <Route path='/state-example' component={ParentState} />
            <Route path='/state-example-multi-rendering' component={ParentStateApp} />
        </Switch>
        </BrowserRouter>

    )
}


export default C5