import React from 'react';


let Employee = ({name, age, designation}) =>{
    return (
        <React.Fragment>
            <nav className="navbar navbar-dark bg-dark navbar-expand-sm p-3">
                <a href="/" className='navbar-brand'>React with props and State</a>
            </nav>
            <section>
                <div className="container mt-3">
                    <p>This is simple props where data transfer from parents to child through props</p>
                    <div className="row">
                        <div className="col-md-8">
                            <div className="card">
                                <div className="card-body bg-light">
                                    Name: {name}  <br />
                                    Age : {age} <br />
                                    Designation: {designation}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </React.Fragment>
    )
}

export default Employee