import React from 'react';
import Customer from "../state-eg/Customer";
import Employee from "./Employee";


let Parent = () =>{
    return (
        <React.Fragment>
            <Employee name="John Vai" age={77} designation="Software Engineer" />
        </React.Fragment>
    )
}

export default Parent