import React, {useState} from 'react';
import CustomerList from "./CustomerList";


let ParentStateApp = () =>{
    let [state, setState] = useState({
        employees: [
            {
                name:'Max K',
                age:28,
                designation:'Programmer',
                location:'Kathmandu'
            },
            {
                name:'Royce',
                age:25,
                designation:'DJ',
                location:'Bhaktapur'
            },

            {
                name:'Wilson',
                age:55,
                designation:'DJ',
                location:'Lalitpur'
            }

        ]
    })
    let {employees} = state
    return (
        <React.Fragment>
            <section>
                <div className="container mt-3">
                    <p>Here we can see the multiple data transfer from parent to child using state</p>
                    <div className="row">
                        {
                            employees.map((employee, index) => {
                                return(
                                    <div className="col-md-3 mt-3" key={index}>
                                        <div className="card">
                                            <img height={300} src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS5lVp0BzQXrxW3jO45hDC3PUo2EDG0Mp6NgmW9nLz1OBHv3nLXPiK7zfhB4vuo7v0pFak&usqp=CAU" alt=""/>
                                            <div className="card-body bg-light">
                                                <ul className='list-group'>
                                                    <li className='list-group-item'>
                                                        Name : {employee.name}
                                                    </li>
                                                    <li className='list-group-item'>
                                                        Age : {employee.age}
                                                    </li>
                                                    <li className='list-group-item'>
                                                        Designation : {employee.designation}
                                                    </li>
                                                    <li className='list-group-item'>
                                                        Address : {employee.location}
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })
                        }


                    </div>
                </div>
            </section>

        </React.Fragment>
    )
}

export default ParentStateApp