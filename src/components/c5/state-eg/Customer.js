import React from 'react';


let Customer = ({name, age, designation}) =>{
    return (
        <React.Fragment>
            <section>
                <div className="container mt-3">
                    <p>This is simple state where state data transfer from parents to child through props</p>
                    <div className="row">
                        <div className="col-md-8">
                            <div className="card">
                                <div className="card-body bg-light">
                                    Name: {name}  <br />
                                    Age : {age} <br />
                                    Designation: {designation}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </React.Fragment>
    )
}

export default Customer