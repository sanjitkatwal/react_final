import React, {useState} from 'react';
import Customer from "./Customer";


let ParentState = () =>{
    let [state, setState] = useState({
        name:'Max K',
        age:28,
        designation:'Programmer'
    })
    return (
        <React.Fragment>
            <Customer name={state.name} age={state.age} designation={state.designation} />
        </React.Fragment>
    )
}

export default ParentState