import React, {useState} from 'react';
import {data} from '../../data'


let CardLitRendering = () =>{
    return (
        <React.Fragment>
            <nav className="navbar navbar-dark bg-dark navbar-expand-sm p-3">
                <a href="/" className='navbar-brand'>--------List Rendering in Card---------</a>
            </nav>
            <pre>{JSON.stringify(data)}</pre>
            <h2>CardLitRendering</h2>
            <section className="mt-3">
                <div className="container">
                    <div className='flex'>
                        <div className='flex-row'>
                            {  data.length > 0 && data.map(employee => {
                                    return(
                            <div className="card">
                                <div className="card-header bg-info">
                                    <p className="h4">{employee.name}</p>
                                </div>
                                <div className="card-body bg-light">
                                    <p className='display-6 m-5'> Email : {employee.email}</p>
                                    <p className='display-6 m-5'>Age : {employee.age}</p>
                                    <p className='display-6 m-5'>Address : {employee.location}</p>
                                </div>
                            </div>
                                    )
                                })
                            }
                        </div>
                    </div>
                </div>
            </section>
        </React.Fragment>
    )
}


export default CardLitRendering