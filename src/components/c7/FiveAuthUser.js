import React, {useState} from 'react';


let AuthUser = ({}) =>{
    let [state, setState] = useState({
        isLoggedIn: false
    })

    let login = () => {
        setState({
            isLoggedIn: true
        })
    }

    let logout = () => {
        setState({
            isLoggedIn: false
        })
    }
    return (
        <React.Fragment>
            <nav className="navbar navbar-dark bg-dark navbar-expand-sm p-3">
                <a href="/" className='navbar-brand'>Auth User</a>
            </nav>
            {JSON.stringify(state)}
            <section>
                <div className="container mt-3">
                    <div className="row">
                        <div className="card">
                            <div className="col-md-12">
                                <div className="card-header bg-info">
                                    <p className="h4">Aiuth User</p>
                                </div>
                                <div className="card-body">
                                    { state.isLoggedIn ? <div onClick={logout} className="btn btn-danger btn-sm m-1">Logout</div> : <div onClick={login} className="btn btn-success btn-sm">Login</div>}

                                    {
                                        state.isLoggedIn?<h1>Welcome User!</h1>:<h1>Thanks for visiting!</h1>
                                    }

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </React.Fragment>
    )
}


export default AuthUser