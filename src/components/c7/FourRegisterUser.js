import React, {useState} from 'react';


let FourRegisterUser = () =>{
    let [state, setState] = useState({
        user: {
            username:'',
            email:'',
            password:'',
            designation:'',
            bio:'',
            terms:false,
        },
        designation : [
            'Software Engineer',
            'Sr. Software Engineer',
            'Tech Lead',
            'project Manager',
            'Director'
        ]
    })

    let {user} = state;

    let changeInput = (event) => {
        setState({
            ...state,
            user: {
                ...user,
                [event.target.name] : event.target.value
            }
        })
    }

    let changeTerms = (event) => {
        setState({
            ...state,
            user: {
                ...user,
                [event.target.name] : event.target.checked
            }
        })
    }
    let submitRegister = (event) => {
        event.preventDefault()
        console.log(state.user)
    }
    return (
        <React.Fragment>
            <nav className="navbar navbar-dark bg-dark navbar-expand-sm p-3">
                <a href="/" className='navbar-brand'>React with Forms handling and RegisterUser</a>
            </nav>
            <h2>FourRegisterUser</h2>
            <pre>{JSON.stringify(state)}</pre>
            <section>
                <div className="container mt-3">
                    <div className="row">
                        <div className="col-md-4">
                            <div className="card">
                                <div className="card-header bg-warning">
                                    <p className="h4">Register User</p>
                                </div>
                                <div className="card-body bg-light">
                                    <form onSubmit={submitRegister}>
                                        <div className="mb-2">
                                            <input type="text"
                                                   name={'username'}
                                                   value={user.username}
                                                   onChange={changeInput}
                                                   placeholder={'Username'}
                                                   className="form-control"/>
                                        </div>
                                        <div className="mb-2">
                                            <input type="text"
                                                   name={'email'}
                                                   value={user.email}
                                                   onChange={changeInput}
                                                   placeholder={'Email'}
                                                   className="form-control"/>
                                        </div>
                                        <div className="mb-2">
                                            <input type="password"
                                                   name={'password'}
                                                   value={user.password}
                                                   onChange={changeInput}
                                                   placeholder={'Password'}
                                                   className="form-control"/>
                                        </div>
                                        <div className="mb-2">
                                            <select name={'designation'}
                                                    onChange={changeInput}
                                                    className="form-control">
                                                <option value=""> Select Designation </option>
                                                {
                                                    state.designation.map((desg, index) => {
                                                        return(
                                                            <option key={index} value={desg} className="form-control">{desg}</option>
                                                        )
                                                    })
                                                }
                                            </select>
                                        </div>
                                        <div className="mb-2">
                                            <textarea rows={3}
                                                      name={'bio'}
                                                      onChange={changeInput}
                                                      placeholder={'Bio'}
                                                      className='form-control'>

                                            </textarea>
                                        </div>
                                        <div className="mb-2">
                                            <input name="terms"
                                                   onChange={changeTerms}
                                                   type="checkbox"
                                                   className='form-check-input'/>Accept Terms
                                        </div>
                                        <input type="submit"
                                               disabled={!user.terms}
                                               className="btn btn-warning btn-sm" value={'Register'}/>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </React.Fragment>
    )
}


export default FourRegisterUser