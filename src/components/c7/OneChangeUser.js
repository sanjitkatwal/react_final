import React, {useState} from 'react';


let OneChangeUser = () =>{
    let [state, setState] = useState({
        username: ''
    })
    let updateUsername = (event) =>{
        setState({
            username: event.target.value
        });
    }
    return (
        <React.Fragment>
            <nav className="navbar navbar-dark bg-dark navbar-expand-sm">
                <a href="/" className='navbar-brand'>React with Forms handling</a>
            </nav>
            <section>
                <div className="container mt-3">
                    <div className="row">
                        <div className="col-md-4">
                            <div className="card">
                                <div className="card-header bg-warning">
                                    <p className="h4">Change User</p>
                                </div>
                                <div className="card-body bg-light">
                                    <form action="">
                                        <div className="mb-2">
                                            <input type="text"
                                                   value={state.username}
                                                   onChange={updateUsername}
                                                   className="form-control"/>
                                        </div>
                                        <p className="h3">{state.username}</p>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </React.Fragment>
    )
}


export default OneChangeUser