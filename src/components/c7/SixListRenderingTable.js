import React, {useState} from 'react';
import {data} from './Data'


let ListRendering = () =>{
    // let [state, setState] = useState({
    //     return 'j'
    // })
    return (
        <React.Fragment>
            <nav className="navbar navbar-dark bg-dark navbar-expand-sm p-3">
                <a href="/" className='navbar-brand text-center'>List Rendering in Table</a>
            </nav>
            {/*<pre>{JSON.stringify(data)}</pre>*/}
            <section>
                <div className="container mt-3">
                    <div className="row">
                        <div className="col">
                            <p className="h4 text-success">Employee List</p>
                            <p>In This section i display the data from api, which is in json format.</p>
                        </div>
                    </div>
                </div>
            </section>
            <section className="mt-3">
                <div className="container">
                    <div className="row">
                        <div className="col">
                            <table className="table table-stripe text-center table-hover">
                                <thead className="bg-success text-white">
                                <tr>
                                    <th>SN</th>
                                    <th>Image</th>
                                    <th>Name</th>
                                    <th>Age</th>
                                    <th>Email</th>
                                    <th>Location</th>
                                </tr>
                                </thead>
                                <tbody>
                                {
                                    data.length > 0 && data.map(contact => {
                                        return(
                                            <tr key={contact.login.uuid}>
                                                <td>{contact.login.uuid.substr(contact.login.uuid.length -5)}</td>
                                                <td> <img src={contact.picture.large} height={50} width={50}/></td>
                                                <td>{contact.name.title} {contact.name.first} {contact.name.last}</td>
                                                <td>{contact.dob.age}</td>
                                                <td>{contact.email}</td>
                                                <td>{contact.location.city}</td>
                                            </tr>
                                        )
                                    })
                                }
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </React.Fragment>
    )
}


export default ListRendering