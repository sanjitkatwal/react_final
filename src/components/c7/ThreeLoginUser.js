import React, {useState} from 'react';


let ThreeLoginUer = () =>{
    let [state, setState] = useState({
        user: {
            username: '',
            password: ''
        }
    })

    let {user} = state;
    let changeInput = (event) => {
        setState({
            user: {
                ...user,
                [event.target.name]: event.target.value
            }
        })
    }
    let submitLogin = (event) => {
        event.preventDefault();
        console.log(state.user)

    }
    // let changePassword = (event) => {
    //     setState({
    //         user: {
    //             ...user,
    //             password: event.target.value
    //         }
    //     })
    // }
    return (
        <React.Fragment>
            <nav className="navbar navbar-dark bg-dark navbar-expand-sm">
                <a href="/" className='navbar-brand'>React with Forms handling</a>
            </nav>
            <pre>{JSON.stringify(state)}</pre>
            <section>
                <div className="container mt-3">
                    <div className="row">
                        <div className="col-md-4">
                            <div className="card">
                                <div className="card-header bg-info">
                                    <p className="h4">Login User</p>
                                </div>
                                <div className="card-body bg-light">
                                    <form onSubmit={submitLogin}>
                                        <div className="mb-2">
                                            <input type="text"
                                                   name={'username'}
                                                   value={user.username}
                                                   onChange={changeInput}
                                                   placeholder={'username'}
                                                   className="form-control"/>
                                        </div>
                                        <div className="mb-2">
                                            <input type="password"
                                                   name={'password'}
                                                   value={user.password}
                                                   onChange={changeInput}
                                                   placeholder={'password'}
                                                   className="form-control"/>
                                        </div>
                                        <input type="submit" className="btn btn-info btn-sm" value={'Login'}/>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </React.Fragment>
    )
}


export default ThreeLoginUer