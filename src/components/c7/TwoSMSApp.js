import React, {useState} from 'react';


let TwoSMSApp = () =>{
    let [state, setState] = useState({
        text: '',
        maxCount: 100
    })

    let updateText = (event) => {
         setState({
             ...state,
             text : event.target.value
         });
    }
    return (
        <React.Fragment>
            <nav className="navbar navbar-dark bg-dark navbar-expand-sm">
                <a href="/" className='navbar-brand'>React with Forms handling with sms</a>
            </nav>
            <section>
                <div className="container mt-3">
                    <div className="row">
                        <div className="col-md-5">
                            <div className="card">
                                <div className="card-header bg-warning">
                                    <p className="h4">SMS App</p>
                                </div>
                                <div className="card-body bg-light">
                                    <pre>{JSON.stringify(state)}</pre>
                                    <form action="">
                                        <div className="mb-2">
                                            <textarea rows={3} value={state.text} maxLength={state.maxCount} onChange={updateText} className="form-control"/>
                                        </div>
                                        <p className="h4">The Characters Remaining :
                                        <small className='fw-bold'>{state.text.length}</small>/100
                                        </p>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </React.Fragment>
    )
}


export default TwoSMSApp