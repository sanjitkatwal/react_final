import React from "react";
import {Link, Route, Switch, BrowserRouter} from "react-router-dom"
import './v7-style.css'
import OneChangeUser from "./OneChangeUser";
import TwoSMSApp from "./TwoSMSApp";
import ThreeLoginUer from "./ThreeLoginUser";
import FourRegisterUser from "./FourRegisterUser";
import FiveAuthUser from "./FiveAuthUser";
import SixListRenderingTable from "./SixListRenderingTable";
import CardLitRendering from "./CardListRendering";

const V7 = () => {
    return(
        <BrowserRouter>
            <h2>Im V7</h2>
            <ul className='nav'>
                <li>
                    <Link to='/onceChangeUser'>One Change user </Link>
                </li>
                <li>
                    <Link to='/twoSMSApp'>Two SMS App </Link>
                </li>
                <li>
                    <Link to='/threeLoginUser'>Three Login user</Link>
                </li>
                <li>
                    <Link to='/fourRegisterUser'>Four Register User</Link>
                </li>
                <li>
                    <Link to='/fiveAuthUser'>Five Auth User</Link>
                </li>
                <li>
                    <Link to='/sixListRenderingTable'>Six list Rendering Table</Link>
                </li>
                <li>
                    <Link to='/card-list-rendering'>Card List</Link>
                </li>
            </ul>
        <Switch>
            <Route path='/onceChangeUser' component={OneChangeUser} />
            <Route path='/twoSMSApp' component={TwoSMSApp} />
            <Route path='/threeLoginUser' component={ThreeLoginUer} />
            <Route path='/fourRegisterUser' component={FourRegisterUser} />
            <Route path='/fiveAuthUser' component={FiveAuthUser} />
            <Route path='/sixListRenderingTable' component={SixListRenderingTable} />
            <Route path='/card-list-rendering' component={CardLitRendering} />
        </Switch>
        </BrowserRouter>

    )
}


export default V7