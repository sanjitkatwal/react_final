import React, {useState} from 'react';


let ChildForms = ({pText, sendData}) =>{
    let [state, setState] = useState({
        cText:''
    })
    let updateChildInput = (event) => {
        setState({
            ...state,
            cText: event.target.value
        })
        sendData(event.target.value);
    }
    return (
        <React.Fragment>
            <div className="container mt-3">
                <div className="row">
                    <div className="col-md-8">
                        <div className="card">
                            <div className="card-body bg-danger text-white">
                                <div className="h4">Child Form</div>
                                <div className="row">
                                    <div className="col-md-4">
                                        <form action="">
                                            <div className="mb-2">
                                                <input
                                                    value={state.cText}
                                                    onChange={updateChildInput}
                                                    type="text"
                                                    className="form-control" placeholder='child data'/>
                                            </div>
                                        </form>
                                        <pre>{state.cText}</pre>
                                        <small>Parent Data:- {pText}</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}


export default ChildForms