import React, {useState} from 'react';
import ChildForms from "./ChildForms";


let ParentForm = () =>{
    let [state, setState] = useState({
        pText: '',
        cText: ''
    })

    let updateInput = (event) => {
        setState({
            ...state,
            pText: event.target.value
        })
    }

    let receiveData = (value) => {
        setState({
            ...state,
            cText: value
        })
    }
    return (
        <React.Fragment>
            <nav className="navbar navbar-dark bg-dark navbar-expand-sm p-3">
                <a href="/" className='navbar-brand'>Parent Child Example with form</a>
            </nav>
            <div className="container mt-3">
                <div className="row">
                    <div className="col">
                        <div className="card">
                            <div className="card-body bg-warning">
                                <div className="h4">Parent Form</div>
                                <div className="row">
                                    <div className="col-md-4">
                                        <form action="">
                                            <div className="mb-2">
                                                <input type="text"
                                                       value={state.pText}
                                                       onChange={updateInput}
                                                       className="form-control" placeholder='parent data'/>
                                            </div>
                                        </form>
                                        <pre>{state.pText}</pre>
                                        <small>Child Data:- {state.cText} </small>
                                    </div>
                                </div>
                                <ChildForms pText={state.pText} sendData={receiveData} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}


export default ParentForm