import React, {useState} from 'react';


let ChildComponent = ({pText, sendData}) =>{
    let [state, setState] = useState({
        cText: 'Im from child'
    })
    let clickSend = () => {
        sendData(state.cText)
    }
    return (
        <React.Fragment>
            <section>
                <div className="container mt-3">
                    <div className="row">
                        <div className="col">
                            <div className="card">
                                <div className="card-body bg-primary text-white">
                                    <p className="h4">ChildCard Component</p>
                                    <small>Parent Data:- {pText} </small>
                                    <button onClick={clickSend} className="btn btn-dark btn-sm">Send</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </React.Fragment>
    )
}


export default ChildComponent