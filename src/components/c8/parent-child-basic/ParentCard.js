import React, {useState} from 'react';
import ChildComponent from "./ChildComponent";


let ParentComponent = () =>{
    let [state, setstate] = useState({
        pText : 'Im from parent',
        cText: ''
    })

    let receiveData = (value) =>{
        setstate({
            ...state,
            cText:value
        })
    }
    return (
        <React.Fragment>
            <nav className="navbar navbar-dark bg-dark navbar-expand-sm p-3">
                <a href="/" className='navbar-brand'>Parent Child property bining</a>
            </nav>
            <section>
                <div className="container mt-3">
                    <div className="row">
                        <div className="col-md-8">
                            <div className="card">
                                <div className="card-body bg-success text-white">
                                    <p className="h4">ParentCard component</p>
                                    <small>Child Data :-{state.cText} </small>
                                    <ChildComponent pText={state.pText} sendData={receiveData} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </React.Fragment>
    )
}


export default ParentComponent