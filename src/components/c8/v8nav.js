import React from "react";
import {Link, Route, Switch, BrowserRouter} from "react-router-dom"
import './v8-nav-style.css'
import ParentForm from "./advanced-parent-child-eg/ParentForm";
import ParentCard from "./parent-child-basic/ParentCard";



const V8nav = () => {
    return(
        <BrowserRouter>
            <h2>Im V8nav</h2>
            <ul className='nav'>
                <li>
                    <Link to='/parent-child-basic'>Parent Child Basic</Link>
                </li>
                <li>
                    <Link to='/parent-child-advanced'>Parent Child Advanced Example</Link>
                </li>

            </ul>
            <Switch>
                <Route path='/parent-child-basic' component={ParentCard}/>
                <Route path='/parent-child-advanced' component={ParentForm}/>
            </Switch>
        </BrowserRouter>

    )
}


export default V8nav