import React from 'react';
import ComponentB from "./ComponentB";


let ComponentA = ({userInfo}) =>{
    return (
        <React.Fragment>
            <div className="container mt-3">
                <div className="row">
                    <div className="col">
                        <div className="car">
                            <div className="card-body bg-info text-white">
                                <p className="h4">Component A</p>
                                <pre>{JSON.stringify(userInfo)}</pre>
                                <ComponentB userInfo={userInfo} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}


export default ComponentA