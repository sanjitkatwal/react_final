import React from 'react';
import ComponentC from "./ComponentC";


let ComponentB = ({userInfo}) =>{
    return (
        <React.Fragment>
            <div className="container mt-3">
                <div className="row">
                    <div className="col">
                        <div className="car">
                            <div className="card-body bg-warning text-white">
                                <p className="h4">Component B</p>
                                <pre>{JSON.stringify(userInfo)}</pre>
                                <ComponentC userInfo={userInfo} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}


export default ComponentB