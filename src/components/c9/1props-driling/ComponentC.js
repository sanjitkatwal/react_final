import React from 'react';
import ComponentD from "./ComponentD";


let ComponentC = ({userInfo}) =>{
    return (
        <React.Fragment>
            <div className="container mt-3">
                <div className="row">
                    <div className="col">
                        <div className="car">
                            <div className="card-body bg-danger text-white">
                                <p className="h4">Component C</p>
                                <pre>{JSON.stringify(userInfo)}</pre>
                                <ComponentD userInfo={userInfo} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}


export default ComponentC