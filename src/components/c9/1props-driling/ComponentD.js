import React from 'react';


let ComponentD = ({userInfo}) =>{
    return (
        <React.Fragment>
            <div className="container mt-3">
                <div className="row">
                    <div className="col">
                        <div className="car">
                            <div className="card-body bg-dark text-white">
                                <p className="h4">Component D</p>
                                <pre>{JSON.stringify(userInfo)}</pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}


export default ComponentD