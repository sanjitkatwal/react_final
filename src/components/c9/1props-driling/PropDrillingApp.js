import React, {useState} from 'react';
import ComponentA from "./ComponentA";


let PropDrillingApp = () =>{
    let [state, setState] = useState({
        userInfo: {
            author:'Sanjit Katwal',
            channel: 'Youtube Tech'

        }
    })
    return (
        <React.Fragment>
            <nav className="navbar navbar-dark bg-dark navbar-expand-sm p-3">
                <a href="/" className='navbar-brand'>Props Drilling</a>
            </nav>
            <div className="container mt-3">
                <div className="row">
                    <div className="col">
                        <div className="car">
                            <div className="card-body bg-success text-white">
                                <p className="h4">App Component for Prop Drilling</p>
                                <pre>{JSON.stringify(state.userInfo)}</pre>
                                <ComponentA userInfo={state.userInfo} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}


export default PropDrillingApp