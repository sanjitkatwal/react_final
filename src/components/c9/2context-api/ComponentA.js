import React from 'react';
import ComponentB from "./ComponentB";


let ComponentA = ({userInfo}) =>{
    return (
        <React.Fragment>
            <div className="container mt-3">
                <div className="row">
                    <div className="col">
                        <div className="car">
                            <div className="card-body bg-info text-white">
                                <p className="h4">Component A</p>
                                <pre></pre>
                                <ComponentB/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}


export default ComponentA