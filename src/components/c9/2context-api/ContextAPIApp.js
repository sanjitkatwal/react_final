import React, {useState} from 'react';
import ComponentA from "./ComponentA";
import UserContext from "./context/UserContext";


let ContextAPIApp = () =>{
    let [state, setState] = useState({
        userInfo: {
            author:'Sanjit Katwal',
            channel: 'Youtube Tech'

        }
    })
    return (
        <React.Fragment>
            <nav className="navbar navbar-dark bg-dark navbar-expand-sm p-3">
                <a href="/" className='navbar-brand'>Context API Concept</a>
            </nav>
            <div className="container mt-3">
                <div className="row">
                    <div className="col">
                        <div className="car">
                            <div className="card-body bg-success text-white">
                                <p className="h4">App Component for ContextAPIApp</p>
                                <pre>{JSON.stringify(state.userInfo)}</pre>
                                {/* context provider */}
                                <UserContext.Provider value={state.userInfo}>
                                    <ComponentA/>
                                </UserContext.Provider>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}


export default ContextAPIApp