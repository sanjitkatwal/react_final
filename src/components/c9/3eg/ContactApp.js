import React, {useState} from "react";
import ContactList from "./ContactList";
import ContactCard from "./ContactCard";
import {Contact} from './Contact'

let ContactApp = () => {

    let [state, setState] = useState({
        selectedContact : ''
    })
    let receiveContact = (contact) => {
        setState({
            selectedContact:contact
        })
    }
    return(
        <React.Fragment>
            <nav className="navbar navbar-dark bg-dark navbar-expand-sm p-3">
                <a href="/" className='navbar-brand'>Example (Context Api)</a>
            </nav>
            {/*<pre>{JSON.stringify(state)}</pre>*/}
            <div className="container mt-3">
                <div className="row">
                    <div className="col">
                        <p className="h4 text-success">Contact App</p>
                        <p>  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab alias aut, commodi consectetur
                            consequatur deleniti dignissimos eaque, esse ex excepturi magnam necessitatibus nobis porro
                            quisquam repellendus reprehenderit sapiente tenetur voluptates.</p>
                    </div>
                </div>
            </div>
            <div className="container mt-3">
                <div className="row">
                    <div className="col-md-9">
                        <ContactList sendContact={receiveContact}/>
                    </div>
                    <div className="col-md-3">
                        <ContactCard selectedContact={state.selectedContact} />
                    </div>

                </div>
            </div>
        </React.Fragment>
    )
}


export default ContactApp