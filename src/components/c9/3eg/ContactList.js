import React, {useState} from 'react';
import {Contact} from './Contact'


let ContactList = ({sendContact}) =>{
    let [state, setState] = useState({
        contacts : Contact
    })
    let {contacts} =state;

    let clickContact = (contact) => {
        sendContact(contact)
        //alert(JSON.stringify(contact))
    }
    return (
        <React.Fragment>
            {/*<pre>{JSON.stringify(Contact)}</pre>*/}
            <h2>Contact App</h2>
            <table className="table table-hover text-center table-striped">
                <thead className='bg-success text-white'>
                <tr>
                    <th>SN</th>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Age</th>
                    <th>Email</th>
                    <th>Location</th>
                </tr>
                </thead>

                <tbody>
                {
                    contacts.length > 0 && contacts.map(contact => {
                        return(
                            <tr key={contact.login.uuid} onClick={()=>clickContact(contact)}>
                                <td>{contact.login.uuid.substr(contact.login.uuid.length -5)}</td>
                                <td> <img src={contact.picture.large} height={50} width={50}/></td>
                                <td>{contact.name.title} {contact.name.first} {contact.name.last}</td>
                                <td>{contact.dob.age}</td>
                                <td>{contact.email}</td>
                                <td>{contact.location.city}</td>
                            </tr>
                        )
                    })
                }
                </tbody>
            </table>
        </React.Fragment>
    )
}


export default ContactList