import React from "react";
import {Link, Route, Switch, BrowserRouter} from "react-router-dom"
import './v9-nav-style.css'
import PropDrillingApp from "./1props-driling/PropDrillingApp";
import ContextAPIApp from "./2context-api/ContextAPIApp";
import ContactApp from "./3eg/ContactApp";



const V9nav = () => {
    return(
        <BrowserRouter>
            <h2>Im V9nav</h2>
            <ul className='nav'>
                <li>
                    <Link to='/props-drilling'>Props Drilling</Link>
                </li>
                <li>
                    <Link to='/context-api'>Context API</Link>
                </li>
                <li>
                    <Link to='/practice-example'>Practice Example</Link>
                </li>

            </ul>
            <Switch>
                <Route path='/props-drilling' component={PropDrillingApp}/>
                <Route path='/context-api' component={ContextAPIApp}/>
                <Route path='/practice-example' component={ContactApp}/>
            </Switch>
        </BrowserRouter>

    )
}


export default V9nav