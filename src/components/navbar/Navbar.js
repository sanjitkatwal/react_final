import React from 'react';
import {BrowserRouter, Link, Route, Switch} from 'react-router-dom';
import './navbar.css'
import Home from "../../Home";
import V7 from "../c7/v7";
import V8nav from "../c8/v8nav";
import V9nav from "../c9/v9nav";
import ComponentApp from "../c4/ComponentApp";
import PropsStatePractice from "../c5/PropsStatePractice";
import C10nav from "../c10/contextApi/c10nav";

const Navbar = () => {
    return (
        <BrowserRouter>
            <nav className='navbar'>
                <h1 className='m-2'>React Notes</h1>
                <ul>
                    <li><Link to="/">Home</Link></li>
                    <li><Link to="/c4">C4</Link></li>
                    <li><Link to="/c5">C5</Link></li>
                    <li><Link to="/v7">C7</Link></li>
                    <li><Link to="/v8">C8</Link></li>
                    <li><Link to="/v9">C9</Link></li>
                    <li><Link to="/c10">C10</Link></li>
                    <li><Link to="/create" style={{color:"white", backgroundColor: '#f56d', borderRadius: '8px'}}>New Blog</Link>
                    </li>
                </ul>
            </nav>
            <Switch>
                <Route path='/' component={Home} exact/>
                <Route path='/c4' component={ComponentApp}/>
                <Route path='/c5' component={PropsStatePractice}/>
                <Route path='/v7' component={V7}/>
                <Route path='/v8' component={V8nav}/>
                <Route path='/v9' component={V9nav}/>
                <Route path='/c10' component={C10nav}/>
            </Switch>
        </BrowserRouter>

    );
};

export default Navbar;

